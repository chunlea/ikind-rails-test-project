class BreedListFetcher
  @@breeds = []

  attr_reader :breeds

  def initialize
    @breeds = []
  end

  def fetch
    @breeds = fetch_info["message"]
  end

  def self.fetch
    return @@breeds unless @@breeds.blank?
    @@breeds = BreedListFetcher.new.fetch
  end

private
  def fetch_info
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => []
    }
  end
end
