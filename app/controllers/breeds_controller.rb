class BreedsController < ApplicationController
  before_action :load_breed_list

  def index
    @breed = DogBreedFetcher.fetch
  end

  def show
    @breed = DogBreedFetcher.fetch(breed_name)
    render :index
  end

  private

  def load_breed_list
    @breeds = BreedListFetcher.fetch
  end

  def breed_name
    name = params[:id].to_s.split("--").join("/")
    name.blank? ? "random" : name
  end

end
